*** Settings ***

Library     OperatingSystem 
Library     hello.py

*** Variables ***

${my_first_var} =   Hello again
${this_is_env} =    %{HOME}
${file1} =  log.html
${file2} =  newDirectory/log.html

*** Keywords ***

I want to print string
    [Arguments]     ${string}
    Log  ${string}    console=yes

I run a python function
    #[Tags]      Test_Case_2_Tag     Another_Tag
    #[Documentation]     This is where we do A/C
    ${this_msg} =   some python function    
    Log To Console      I run python keyword: ${this_msg}

I want to copy a file   
    [Arguments]     ${file1}    ${file2}
    Copy File   ${file1}    ${file2}

*** Test Cases ***

SPAM_123
    [Tags]      AC2.1  
    [Documentation]     You have to run a python function
    I run a python function

