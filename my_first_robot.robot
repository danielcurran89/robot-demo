*** Settings ***

Library     OperatingSystem 
Library     hello.py

*** Variables ***

${my_first_var} =   Hello again
${this_is_env} =    %{HOME}
${file1} =  log.html
${file2} =  newDirectory/log.html

*** Keywords ***

I want to print string
    [Arguments]     ${string}
    Log  ${string}    console=yes

I run a python function
    #[Tags]      Test_Case_2_Tag     Another_Tag
    #[Documentation]     This is where we do A/C
    ${this_msg} =   some python function    
    Log To Console      I run python keyword: ${this_msg}

I want to copy a file   
    [Arguments]     ${file1}    ${file2}
    Copy File   ${file1}    ${file2}

*** Test Cases ***

SPAM_001
    [Tags]      AC1.1  
    [Documentation]     You need to be able to print strings
    I want to print string     ${my_first_var}
    I want to print string      ${this_is_env}

SPAM_002
    [Tags]      AC1.2
    I run a python function

SPAM_003
    Copy File   ${file1}    ${file2}
    File Should Exist   ${file2}
