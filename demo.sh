#!/bin/bash

# ***************************************************
# Do some stuff
# ***************************************************

# ***************************************************
# Install dependencies
# ***************************************************
printf "===================================================\n"
printf "Installing dependencies\n"
printf "===================================================\n"
#pip3 install robotframework
#pip3 install --upgrade robotframework-sshlibrary

printf "\n"

python3 -m robot my_first_robot.robot
#python3 -m robot my_second_robot.robot
python3 -m robot -o newfolder/output2.xml my_second_robot.robot
python3 -m robot.rebot -r finaloutput.html output.xml newfolder/output2.xml 

